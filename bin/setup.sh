#!/bin/bash
SUBDIR="$PWD/$(dirname "$0")"
set -x

mkdir -p ~/bin
ln -sf "$SUBDIR/bin/"* ~/bin/
