#!/bin/bash
set -eux

sudo pacman -S --needed rustup

if ! pacman -Q paru; then

	rustup default nightly

	cd /tmp
	rm -rf paru
	git clone https://aur.archlinux.org/paru.git
	cd paru
	makepkg -si

	cd /tmp
	rm -rf paru

fi

