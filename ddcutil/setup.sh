#!/bin/bash
set -x

paru -S --needed i2c-tools

if [ ! -f /etc/udev/rules.d/45-ddcutil-i2c.rules ]; then
	echo "KERNEL==\"i2c-[0-9]*\", GROUP=\"$USER\", MODE=\"0660\", PROGRAM=\"/usr/bin/ddcutil --bus=%n getvcp 0x10\"" | sudo tee /etc/udev/rules.d/45-ddcutil-i2c.rules
fi

if [ ! -f /etc/modules-load.d/i2c-dev.conf ]; then
	echo "i2c-dev" | sudo tee /etc/modules-load.d/i2c-dev.conf
fi
