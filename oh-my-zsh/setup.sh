#!/bin/bash
SUBDIR="$PWD/$(dirname "$0")"
set -x

if ! pacman -Q zsh; then
	pacman -S zsh
fi

if [ ! -x ~/.oh-my-zsh ]; then
	git clone --recurse https://github.com/CromFr/crom-custom-oh-my-zsh.git ~/.oh-my-zsh
	cp ~/.oh-my-zsh/templates/zshrc.zsh-template ~/.zshrc
fi
