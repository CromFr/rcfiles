#!/bin/bash
swaymsg -t get_tree | jq -r '.nodes[].nodes[] | if .nodes then [recurse(.nodes[])] else [] end + .floating_nodes | .[] | select(.nodes==[]) | ("NAME=" + .name + " APPID=" + .app_id + " WPROP=" + (.window_properties | tostring ) )'

