#!/bin/bash

set -euo pipefail

# Install script
cat << EOF > /tmp/locktrap.sh
#!/bin/bash
I=0
while [ -f "/tmp/locktrap-culprit_\$I" ]; do
	I=\$((I + 1))
done

while [ -f /tmp/locktrap-capturing ]; do
	if [[ "\$(cat /tmp/locktrap-capturing)" == 1 ]]; then
		gst-launch-1.0 v4l2src num-buffers=10 ! jpegenc ! filesink location=/tmp/locktrap-culprit_\$I.jpg
		I=\$((I + 1))
	else
		sleep 0.5
	fi
done
EOF
chmod +x /tmp/locktrap.sh

# process handling capture
echo 0 > /tmp/locktrap-capturing
/tmp/locktrap.sh&
CAP_PID=$!

# Start/stop the capture
swayidle timeout 2 'echo 0 > /tmp/locktrap-capturing' resume 'echo 1 > /tmp/locktrap-capturing'&
IDLE_PID=$!

# Create lock images
LOCKARGS=""
for OUTPUT in $(swaymsg -t get_outputs | jq -r '.[].name'); do
    IMAGE="/tmp/locktrap-img-$OUTPUT.jpg"
    grim -o "$OUTPUT" "$IMAGE"
    LOCKARGS="${LOCKARGS} --image ${OUTPUT}:${IMAGE}"
done
sleep 0.1

# Lock
swaylock --ignore-empty-password --no-unlock-indicator $LOCKARGS

# Stop processes
kill $CAP_PID
kill $IDLE_PID
wait

# Cleanup
rm -f /tmp/locktrap-capturing
# rm -f /tmp/locktrap-img-*.jpg
rm -f /tmp/locktrap.sh
