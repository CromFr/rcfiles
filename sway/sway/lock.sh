#!/bin/bash
set -euo pipefail

if pgrep --exact swaylock > /dev/null; then
	echo "[$(basename "$0")] Already locked"
	exit 0
fi

NO_DPMS=${NO_DPMS:-}
if [[ "$(hostname)" == "tcharles-parifex" ]]; then
	NO_DPMS=1
fi

if [ -z $NO_DPMS ]; then
	swayidle -w timeout 10 'swaymsg "output * dpms off"' resume 'swaymsg "output * dpms on"'&
	SWAYIDLE_PID=$!
else
	BRIGHTNESS=$(brightnessctl get)
	swayidle -w timeout 10 "brightnessctl set 0 >/dev/null" resume "brightnessctl set $BRIGHTNESS >/dev/null"&
	SWAYIDLE_PID=$!
fi

if [ -n "$SWAYIDLE_PID" ]; then
	trap "kill $SWAYIDLE_PID" EXIT
fi

# Lock screen
echo "[$(basename "$0")] Locking now !"
swaylock --show-failed-attempts --ignore-empty-password "$@"

