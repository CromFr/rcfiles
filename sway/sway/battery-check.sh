#!/bin/bash
set -euo pipefail

while :; do
	BATT=$(acpi -b)
	if grep -q -i 'discharging' <<< "$BATT" && [[ "$(cut -f 5 -d " " <<< "$BATT")" < "00:10:00" ]] ; then
	    notify-send -i "/usr/share/icons/Adwaita/scalable/status/battery-level-0-symbolic.svg" -u critical "Low battery !" "$BATT"
	fi

	sleep 120
done
