#!/bin/bash
set -euo pipefail
set -x

DELAY=${1:-20}
MESSAGE="Will lock in ${DELAY} seconds"

function lock_if_alive(){
	if kill -0 "$1" >& /dev/null; then
		# Still running, lock
		echo "[$(basename "$0")] swayidle is still running, lock now"
		kill "$1" || true
		loginctl lock-session
	fi
}

echo "[$(basename "$0")] notify"
notify-send \
	-t "${DELAY}500" \
	-u critical \
	-i /usr/share/icons/HighContrast/48x48/status/system-lock-screen.png \
	"$MESSAGE"
NOTIFID=$(makoctl list \
	| jq -r '.data | flatten | .[0] | select(.summary.data == "'"$MESSAGE"'") | .id.data')


timeout $((DELAY + 1)) swayidle -w timeout 0 "true" resume "pkill -n swayidle" &
TIMEOUT_PID=$!
SWAYIDLE_PID=$(pgrep --parent $TIMEOUT_PID)

# Set swayidle in idle state
kill -s USR1 "$SWAYIDLE_PID"

# Lock if TIMEOUT_PID is still up after $DELAY
(sleep "$DELAY" && lock_if_alive $TIMEOUT_PID)&
DELAYLOCK_PID=$!

# pstree -p | grep -C2 -E "swaylock|swayidle|warn-lock"

wait $TIMEOUT_PID || true
echo "[$(basename "$0")] Delay elapsed / bypassed"

makoctl dismiss -n "$NOTIFID"

kill $DELAYLOCK_PID >&/dev/null || true
