#!/bin/bash
set -euo pipefail
if ! sway --get-socketpath > /dev/null; then
	export SWAYSOCK=$(echo /run/user/$UID/sway-ipc.$UID.*.sock)
	echo "Using $SWAYSOCK instead"
fi

swaymsg -t get_tree \
	| jq -r '
	def lpad(n):
		tostring
		| if (n > length) then ((n - length) * " ") + . else . end;

	.nodes[].nodes[] | if .nodes then [recurse(.nodes[])] else [] end + .floating_nodes | .[] | select(.nodes==[]) | ("<small>" + (.app_id | lpad(16)) + "</small> " + .name)' \
	| rofi "$@" -markup -markup-rows -dmenu \
	| ( read -r id name && swaymsg "[con_id=$id]" focus )


