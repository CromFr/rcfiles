#!/bin/bash
set -euo pipefail
if ! sway --get-socketpath > /dev/null; then
	export SWAYSOCK=$(echo /run/user/$UID/sway-ipc.$UID.*.sock)
	echo "Using $SWAYSOCK instead"
fi

OUTPUTS=$(swaymsg -t get_outputs)

# Default laptop screen
INTERNAL_OUTPUT=$(echo "$OUTPUTS" | jq -r '.[] | select(.serial == "0x00000000") | .name')

# Connected displays
EXTERNAL_OUTPUTS=$(echo "$OUTPUTS" | jq -r ".[].name | select(. != \"$INTERNAL_OUTPUT\")" | tr '\n' ' ' || true)

echo "Internal output: $INTERNAL_OUTPUT"
echo "External outputs: $EXTERNAL_OUTPUTS"

function set-external(){
	local out
	for out in $EXTERNAL_OUTPUTS; do
		swaymsg output "$out" "$1"
	done
}

function detect-setups(){
	local outputs; outputs=$(swaymsg -t get_outputs)
	local internal; internal=$(echo "$outputs" | jq -r '.[] | select(.name | test("LVDS-.*")) | .name')
	local externals; externals=$(echo "$outputs" | jq -r '.[] | select(.name | test("LVDS-.*") == false) | select(.active) | .make + " " + .model + " " + .serial' | sort)

	local setup; setup=$(echo -n "$externals" | tr '\n' '+')
	echo "Screen setup ID: '$setup'"

	case "$setup" in
		'Dell Inc. S2719DGF GFSN7P2+Samsung Electric Company SyncMaster H9XQB02409')
			echo "Detected home desktop dual screen setup"
			set -x
			swaymsg output "'Dell Inc. S2719DGF GFSN7P2'" pos 1920 0
			swaymsg output "'Samsung Electric Company SyncMaster H9XQB02409'" pos 0 100
			swaymsg output "$internal" pos $((1920 + (2560 - 1920) / 2)) 1440
			#(ddcutil setvcp -l S2719DGF 60 0x12 2>/dev/null)& # 0f: DisplayPort-1 11: HDMI-1 12: HDMI-2
			#(ddcutil setvcp -l SyncMaster 60 0x01 2>/dev/null)& # 01: VGA-1 03: DVI-1
			(ddcutil setvcp -l SyncMaster 10 40 2>/dev/null)&
			;;
		'Dell Inc. S2719DGF GFSN7P2')
            echo "Detected home desktop single screen setup"
            set -x
            swaymsg output "'Dell Inc. S2719DGF GFSN7P2'" pos 0 0
			swaymsg output "$internal" pos $(( (2560 - 1920) / 2 )) 1440
			#(ddcutil setvcp -l S2719DGF 60 0x12 2>/dev/null)& # 0f: DisplayPort-1 11: HDMI-1 12: HDMI-2
            ;;
		*)
			echo "Unknown setup"
			;;
	esac

	wait
}

function ddc_switch(){
	local ddc=$(ddcutil detect --nousb --sleep-multiplier=0.1 --brief 2>/dev/null)

	local buses=$(echo "$ddc" | grep -o "I2C bus:\s.*$" | sed -E 's/I2C bus:\s+(.+)$/\1/g')
	local ids=$(echo "$ddc" | grep -o "Monitor:\s.*$" | sed -E 's/Monitor:\s+.+:(.+):(.+)$/\1_\2/g')

	for i in {0..$(echo -n "buses" | wc -l)}; do
		local "$(echo "$ids" | head -n$((i + 1)) | tail -n1)=$(echo "$buses" | head -n$((i + 1)) | tail -n1)"
	done

	while read -r cmd; do
		if [ -z "$cmd" ]; then
			continue
		fi


	done

}

ANS="${1:-}"
if [ -z "$ANS" ]; then
	ANS=$(echo -e "laptop\nexternal\nall\nvga-enable\nvga-disable" | wofi --dmenu -i --prompt "Internal: $INTERNAL_OUTPUT External: $EXTERNAL_OUTPUTS")
fi

echo "Selected $ANS"
case "$ANS" in
    laptop)
		swaymsg output $INTERNAL_OUTPUT enable
		set-external disable
		;;
    external)
		swaymsg output $INTERNAL_OUTPUT disable
		set-external enable
		;;

    all)
		swaymsg output $INTERNAL_OUTPUT enable
		set-external enable
		;;

    vga-enable)
		swaymsg output $(swaymsg -t get_outputs | jq -r '.[].name | select(test("VGA-[0-9]+"))') enable
		;;
	vga-disable)
		swaymsg output $(swaymsg -t get_outputs | jq -r '.[].name | select(test("VGA-[0-9]+"))') disable
		;;
	*) exit 0 ;;
esac

#detect-setups

