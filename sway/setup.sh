#!/bin/bash
SUBDIR="$PWD/$(dirname "$0")"
set -x

gpg --list-keys F4B432D5D67990E3 || gpg --keyserver pgp.mit.edu --recv-keys F4B432D5D67990E3 # wob
paru -S --needed \
	sway swaylock swaybg swayidle alacritty waybar wofi ddcutil brightnessctl playerctl gnome-calculator grim wl-clipboard kdeconnect mako xorg-xwayland sysstat python-i3ipc xdg-desktop-portal-wlr pamixer slurp gammastep python-psutil pipewire-media-session xdg-desktop-portal-wlr acpi \
	wob ttf-firacode-nerd kanshi-git pantheon-polkit-agent

if ! command -v ufw ; then
	# Allow kdeconnect through UFW
	if ! grep -qE "allow\s+(tcp|udp)\s+1714:1764" /etc/ufw/user.rules; then
		sudo ufw allow 1714:1764/tcp
		sudo ufw allow 1714:1764/udp
	fi
fi

ln -sf "$SUBDIR/"*/ ~/.config/
ln -sf "$SUBDIR/wob.ini" ~/.config/
mkdir -p ~/.config/environment.d/
ln -sf "$SUBDIR/sway/env/0-wayland.conf" ~/.config/environment.d/

ln -sf "$SUBDIR/sway/bin/"* ~/bin

mkdir -p ~/.config/systemd/user/kanshi.service.d
ln "$SUBDIR/kanshi.service" ~/.config/systemd/user/
ln "$SUBDIR/kanshi.service.override.conf" ~/.config/systemd/user/kanshi.service.d/

if [[ $(shasum shasum /usr/share/wayland-sessions/sway-env.desktop | cut -d' ' -f1) == $(shasum $SUBDIR/sway-env.desktop | cut -d' ' -f1) ]]; then
	echo "Installing sway-env.desktop"
	sudo cp $SUBDIR/sway-env.desktop /usr/share/wayland-sessions/sway-env.desktop
fi

