#!/usr/bin/env python
import psutil
import math
import sys


char_map = {
	0: "▁",
	1: "▂",
	2: "▃",
	3: "▄",
	4: "<span foreground=\"#ff0\">▅</span>",
	5: "<span foreground=\"#ff0\">▆</span>",
	6: "<span foreground=\"#f00\">▇</span>",

	7: "<span foreground=\"#f00\">█</span>",
	8: "<span foreground=\"#f00\">█</span>",
};

while True:
	usage = [
		char_map.get(math.floor(u / 12.5), "?")
		for u in psutil.cpu_percent(interval=1, percpu=True)
	]
	print("".join(usage))
	sys.stdout.flush()

