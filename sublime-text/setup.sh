#!/bin/bash
SUBDIR="$PWD/$(dirname "$0")"

set -e

if ! grep -q '\[sublime-text\]' /etc/pacman.conf; then
	echo -e "\n[sublime-text]\nServer = https://download.sublimetext.com/arch/stable/x86_64" | sudo tee -a /etc/pacman.conf
	curl -O https://download.sublimetext.com/sublimehq-pub.gpg
	sudo pacman-key --add sublimehq-pub.gpg
	sudo pacman-key --lsign-key 8A8F901A
	rm sublimehq-pub.gpg
fi

paru -S --needed sublime-text/sublime-text sublime-text/sublime-merge jq python-hjson

SUBL_VERSION=$(subl --version | grep -oE '[0-9]+$')
if (( SUBL_VERSION >= 4000 )); then
	CFG_PATH=~/.config/sublime-text
else
	CFG_PATH=~/.config/sublime-text-3
fi

PACKAGES=$(echo -n \
'Alignment
Emmet
File Rename
GitSavvy
LoremIpsum
MarkdownEditing
MarkdownPreview
Material Theme
PackageDev
PackageResourceViewer
SideBarEnhancements
Table Editor
Wrap Plus
RainbowBrackets
UnicodeCompletion
A File Icon
Color Highlighter' | sort)

if [ ! -f "$CFG_PATH/Packages/User/Package Control.sublime-settings" ]; then
	subl
	echo "Install Package control and press enter to continue"
	read
fi

INSTALLED_PACKAGES=$( (cat "$CFG_PATH/Packages/User/Package Control.sublime-settings" | hjson -c | jq -r '.installed_packages[]' | sort ) || true)

PACKAGES_TO_INSTALL=$(comm -23 <(echo -n "$PACKAGES") <(echo -n "$INSTALLED_PACKAGES") | jq --raw-input --slurp 'split("\n")')

subl --command 'advanced_install_package {"packages": '"$PACKAGES_TO_INSTALL"' }'
# echo "Install the following packages:"
# echo Alignment
# echo Emmet
# echo File Rename
# echo GitSavvy
# echo LoremIpsum
# echo MarkdownEditing
# echo MarkdownPreview
# echo Material Theme
# echo PackageDev
# echo PackageResourceViewer
# echo SideBarEnhancements
# echo Table Editor
# echo Wrap Plus
# read


set -x
mkdir -p "$CFG_PATH/Packages/User"
ln -sf "$SUBDIR/UserSettings/"* "$CFG_PATH/Packages/User"

