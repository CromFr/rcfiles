#!/bin/bash
SUBDIR="$PWD/$(dirname "$0")"
set -xeu

CPUS=$(lscpu | awk '/^CPU\(s\):\s*([0-9]+)/ {print $2}')

if ! grep -E "^ILoveCandy$" /dev/null; then
	sudo sed -i 's|\[options\]|[options]\nILoveCandy|' /etc/pacman.conf
fi

sudo sed -i 's|^#VerbosePkgLists|VerbosePkgLists|' /etc/pacman.conf
sudo sed -i 's|^#Color|Color|' /etc/pacman.conf
sudo sed -i 's|^#ParallelDownloads|ParallelDownloads|' /etc/pacman.conf

sudo sed -Ei 's|^#HandleLidSwitch=.*$|HandleLidSwitch=lock|' /etc/systemd/logind.conf
sudo sed -Ei 's|^#HandlePowerKey=.*$|HandlePowerKey=ignore|' /etc/systemd/logind.conf

sudo sed -Ei 's|^#MAKEFLAGS=.*$|MAKEFLAGS="-j$(nproc)"|' /etc/makepkg.conf
sudo sed -Ei 's|-march=[0-9a-zA-Z_-]+|-march=native|' /etc/makepkg.conf
sudo sed -Ei 's|-mtune=[0-9a-zA-Z_-]+|-mtune=native|' /etc/makepkg.conf
sudo sed -Ei 's|COMPRESSXZ=\(xz -c -z -\)|COMPRESSXZ=(xz -T$(nproc) -c -z -)|' /etc/makepkg.conf

# Activate numlock in TTY
if [ ! -f /etc/systemd/system/getty@.service.d/activate-numlock.conf ]; then
	sudo mkdir -p /etc/systemd/system/getty@.service.d/
	sudo cp "$SUBDIR/activate-numlock.conf" /etc/systemd/system/getty@.service.d/
fi


# Tune login lockout
sudo sed -Ei 's|^#\s*deny\s*=.*$|deny = 5|' /etc/security/faillock.conf
sudo sed -Ei 's|^#\s*unlock_time\s*=.*$|unlock_time = 300|' /etc/security/faillock.conf

# Install mirrorlist hook
sudo mkdir -p /etc/pacman.d/hooks
sudo cp "$SUBDIR/pacman-mirrorlist.hook" /etc/pacman.d/hooks/

