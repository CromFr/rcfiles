#!/bin/bash

echo -n ".gitconfig: [C]rom or [W]ork? "
read RES

if [[ "$RES" == "C" ]]; then
	set -x
	git config --global user.name "Crom (Thibaut CHARLES)"
	git config --global user.email "CromFr@gmail.com"
elif [[ "$RES" == "W" ]]; then
	set -x
	git config --global user.name "Thibaut CHARLES"
    git config --global user.email "Thibaut.Charles@smile.fr"
else
	"$0"
	exit
fi
git config --global user.signingkey "0x8A4BD04DA8BB324720C1B842568E61BBFB6DA047"
git config --global commit.gpgsign true
git config --global core.editor vim
git config --global merge.tool meld
git config --global pull.rebase false
git config --global rebase.autostash true
git config --global init.defaultBranch main

git config --global alias.lg1 "log --graph --abbrev-commit --decorate --date=relative --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(bold yellow)%d%C(reset)' --all"
git config --global alias.lg2 "log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold cyan)%aD%C(reset) %C(bold green)(%ar)%C(reset)%C(bold yellow)%d%C(reset)%n'' %C(white)%s%C(reset) %C(dim white)- %an%C(reset)' --all"
git config --global alias.lg '!git lg1'
git config --global alias.loop '!git commit -m \"see commit $(git log --format=format:%H | shuf | head -n1)\"'
git config --global alias.vomit '!head /dev/urandom | tr "\\0" 0 | git commit --file=-'
git config --global alias.fuck '!curl -s http://whatthecommit.com/index.txt | git commit --file=-'
git config --global alias.pushu '!git push -u origin "$(git branch --show-current)"'
git config --global alias.pushf 'push --force-with-lease'

