#!/bin/bash

echo "Paste here content of 'gpg --export-secret-keys --armor cromfr@gmail.com'"
gpg --import --armour

# Trust key
echo "trust\n5\ny" | gpg --command-fd 0 --edit-key 8A4BD04DA8BB324720C1B842568E61BBFB6DA047
