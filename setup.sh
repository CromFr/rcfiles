#!/bin/bash

set -euo pipefail

for TOOL in */; do
	echo "======================================================================="
	echo -n "Setup $TOOL? y/n "
	read ANS
	if [[ "$ANS" == "y" || "$ANS" == "" ]]; then
		$TOOL/setup.sh
	else
		echo "Skipped $TOOL"
	fi
done
