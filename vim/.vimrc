source $VIMRUNTIME/vimrc_example.vim

set nocompatible

set pastetoggle=<F2>
"set number
set nowrap
set tabstop=4
set backspace=indent,eol,start
set autoindent
set copyindent
set shiftwidth=4
set shiftround
set showmatch
set ignorecase
set smartcase
set smarttab
set hlsearch
set incsearch

set mouse=a
syntax enable
colorscheme monokai
