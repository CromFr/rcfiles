#!/bin/bash
SUBDIR="$PWD/$(dirname "$0")"
set -x

ln -sf "$SUBDIR/.vimrc" ~/.vimrc
ln -sf "$SUBDIR/.vim" ~/.vim