#!/bin/bash
set -e

if pgrep i3lock; then 
	echo "Already locked (i3lok already running)"
	exit 0
fi

#echo "suspend message display"
pkill -u "$USER" -USR1 dunst

SUSPEND=0
if [[ "$1" == "suspend" ]]; then
	#echo "Suspend mode"
	SUSPEND=1
	shift
fi

NOBLANK=0
if [[ "$1" == "noblank" ]]; then
	NOBLANK=1
	shift
fi

dbus-send --print-reply --dest=com.github.chjj.compton.${DISPLAY/:/_} / \
    com.github.chjj.compton.opts_set string:no_fading_openclose boolean:false ||true

i3lock -e -n -f -t \
        --blur=sigma \
        --pass-media-keys \
        --refresh-rate=0.1 \
        --clock \
        --timepos="x +w/2:y +h-50" \
	--datestr="%A %d/%m/%Y" \
        --date-font="Fira" --time-font="Fira" \
        --datesize=24 --timesize=44 \
        --datecolor=ffffffff  --timecolor=ffffffff \
        \
        --indpos="x +w/2-200:y +h-50" --radius=25 --ring-width=15 \
        --insidecolor=00000000 --ringcolor=ffffffff --line-uses-inside --separatorcolor=ff0000ff \
        --keyhlcolor=006D97ff --bshlcolor=d23c3dff --separatorcolor=00000000 \
        --insidevercolor=196D7880 --insidewrongcolor=d23c3dff \
        --ringvercolor=ffffffff --ringwrongcolor=ffffffff \
        --wrongtext "Try harder..." \
        "$@" &

LOCK_PID=$!

sleep 0.5

if (( SUSPEND )); then
	systemctl suspend -i
else
	if (( NOBLANK == 0 )); then
		(
			sleep 4
			if kill -0 "$LOCK_PID"; then
				xset dpms force suspend &
			fi
		)&
	fi
fi

wait

#echo "resume message display"
pkill -u "$USER" -USR2 dunst

dbus-send --print-reply --dest=com.github.chjj.compton.${DISPLAY/:/_} / \
    com.github.chjj.compton.opts_set string:no_fading_openclose boolean:true ||true
