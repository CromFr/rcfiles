#!/bin/bash

set -euo pipefail

if [[ "$1" == "save" ]]; then
	xbacklight > /tmp/.xbacklight-save
elif [[ "$1" == "restore" ]]; then
	xbacklight -set "$(cat /tmp/.xbacklight-save)"
	rm /tmp/.xbacklight-save
else
	exit 1
fi
