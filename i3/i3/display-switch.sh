#!/bin/bash
set -euo pipefail

# Default laptop screen
INTERNAL_OUTPUT="LVDS-0"

# Connected displays
EXTERNAL_OUTPUT=$(xrandr | grep -oP '^(?!LVDS-0)[A-Z0-9\-]+\s+connected' | cut -d " " -f1)


ANS=$(echo -e "laptop\nexternal\ndual-optional\ndual-main\nclone-optional\nclone-main" | rofi -dmenu -i)

# xrander will run and turn on the display you want, if you have an option for 3 displays, this will need some modifications
case "$ANS" in
    laptop)
		xrandr \
			--output "$INTERNAL_OUTPUT" --auto --primary \
			--output "$EXTERNAL_OUTPUT" --off
		;;
    external)
		xrandr \
			--output "$INTERNAL_OUTPUT" --off \
			--output "$EXTERNAL_OUTPUT" --primary --auto
		;;

    dual-optional)
		xrandr \
			--output "$INTERNAL_OUTPUT" --auto --primary \
			--output "$EXTERNAL_OUTPUT" --auto --right-of "$INTERNAL_OUTPUT"
		;;
    dual-main)
		xrandr \
			--output "$INTERNAL_OUTPUT" --auto \
			--output "$EXTERNAL_OUTPUT" --auto --right-of "$INTERNAL_OUTPUT" --primary
		;;

	# TODO: scale resolutions
    clone-optional)
		xrandr \
			--output "$INTERNAL_OUTPUT" --auto \
			--output "$EXTERNAL_OUTPUT" --auto --mode 1920x1080
		;;
    clone-main)
		xrandr \
			--output "$INTERNAL_OUTPUT" --auto --same-as "$INTERNAL_OUTPUT" --mode 2560x1440 \
			--output "$EXTERNAL_OUTPUT" --auto
		;;
	*) exit 0 ;;
esac



