#!/bin/bash
SUBDIR="$PWD/$(dirname "$0")"
set -x


PKGLIST="i3-gnome compton dunst feh xautolock rofi polybar i3lock-color conky otf-nerd-fonts-fira-code xss-lock"
if ! pacman -Q $PKGLIST; then
	paru -S --needed i3-gnome compton dunst feh xautolock rofi polybar i3lock-color conky otf-nerd-fonts-fira-code xss-lock
fi


for F in "$SUBDIR/"*/; do
	ln -sf "$F" ~/.config
done
