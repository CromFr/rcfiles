#!/usr/bin/env bash

set -euo pipefail

# Terminate already running bar instances
killall -q polybar || true

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 0.5; done

cd ~/.config/polybar

for m in $(polybar --list-monitors | cut -d":" -f1); do
	MONITOR=$m polybar "$@" top &
done

wait
