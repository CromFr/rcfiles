#!/bin/bash
# Packages I typically need when installing a new Archlinux

PKGS=" \
base base-devel linux linux-firmware \
gnome gnome-extra seahorse \
networkmanager netctl dhcpcd dialog wpa_supplicant inetutils \
parted ntfs-3g lvm2 cryptsetup \
grub efitools efibootmgr \
ddcutil \
openssh openvpn ufw \
zsh vim git bat fd exa tree pass htop rsync sshfs man jq wget ripgrep \
firefox browserpass-firefox vlc filezilla tilix ttf-fira-code \
element-desktop discord \
evtest \
"

PKGS_INTEL=" \
intel-ucode \
mesa xf86-video-intel vulkan-intel intel-media-driver \
"

PKGS_AUR=" \
sublime-text-3 \
"


